Gaptek Mempuni (Gerakan Pemanfaatan Teknologi Informasi Untuk Meningkatkan Pelayanan Bagian Umum dan Protokoler Pemerintah Kota Cimahi)
Merupakan sistem informasi berbasis web yang dibangun untuk bagian umum dan protokoler pemerintah kota cimahi.
Aplikasi ini dibangun dengan menggunakan Framework PHP Codeigniter 3.0 dan Library pendukung seperti Ajax, JQuery, dan Database MySQL

# Fitur:
- Codeigniter 3.0
- DB MySQL
- JQuery

# Modul:
1. Modul Peminjaman Ruangan
2. Modul Peminjaman Peralatan
3. Modul Pengajuan Barang Habis Pakai
4. Modul Kelola Pengajuan (Peminjaman Ruangan, Peralatan, dan Barang Habis Pakai)
5. Modul Kelola Ruangan 
6. Modul Kelola Peralatan (Inventarisir)
7. Modul Lihat Jadwal Pengajuan
8. Modul Kelola Pengguna
9. DLL