SIMKESWAN (Sistem Informasi Pusat Kesehatan Hewan Kota Cimahi)
Merupakan sistem informasi berbasis web yang dibangun untuk pengelolaan kegiatan pengobatan di Pusat Kesehatan Hewan Kota Cimahi
Aplikasi ini dibangun dengan menggunakan Framework PHP Codeigniter 3.0 dan Library pendukung seperti Ajax, Javascript/JQuery, dan Database MySQL

# Fitur:
- Codeigniter 3.0
- DB MySQL
- JQuery
- Scan QR Code 

# Modul:
1. Modul Pendaftaran dan Antrian
2. Modul Kelola Pasien
3. Modul Kelola Rawat Inap
4. Modul Kelola Tindakan 
5. Modul Kelola Jenis Hewan
6. Modul Kelola Diagnosa 
7. Modul Kelola Alat Medis (Inventarisir)
8. Modul Kelola Obat
9. Modul Kategori Obat
10. Modul Kelola laporan 
11. Modul Kelola Dokter
12. Modul Kelola Pengguna
13. Modul Detail Pasien (Cetak Kartu Pasien, Cetak Rekam Medis, Input Hasil Pemeriksaan Pasien)
14. DLL