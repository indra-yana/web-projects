Purchasing (PT Centrin Online Prima)
Merupakan sistem informasi berbasis web yang dibangun untuk membuat pengajuan Purchase Request secara online.
Aplikasi ini dibangun dengan menggunakan Framework PHP Laravel 5.8 dan Library pendukung seperti Ajax, JQuery, dan Database MySQL

# Fitur:
- Laravel 5.8
- DB MySQL
- JQuery
- User Previlege (Role & Permission)
- Datatable Server Side (Lightweight to load data)

# Modul:
1. Modul Dashboard
2. Modul Purchase Request (CRUD, Histori/Timeline Progress PR, Multiple Add PR Items, Multiple Upload Document)
3. Modul Supplier (CRUD)
4. User Management (Administrator)
	- Role Management
	- Permission Management
	- User Management (CRUD, Asign Role, Add Permission)
5. Modul Branch Management