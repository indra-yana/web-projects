Employee Management System (PT Centrin Online Prima)
Merupakan sistem informasi berbasis web yang dibangun untuk pengelolaan data karyawan.
Aplikasi ini dibangun dengan menggunakan Framework PHP Laravel 5.7 dan Library pendukung seperti Ajax, JQuery, dan Database MySQL

# Fitur:
- Laravel 5.7
- DB MySQL
- JQuery
- Role Based Permission (Employee, Admin-HR, Super-Admin)
- Multi Auth Guard
- Export/Import data to Excel
- Datatable Server Side (Lightweight to load data)
- Import Attendance from fingerprint data
- Forgot Password

# Modul:
1. Modul Dashboard
2. Modul Employee List
3. Modul Employee Chief
4. Modul Attendance (Untuk pengelolaan kehadiran)
	- Manual Add or Edit attendance
	- Import/Export Attendance dari data fingerprint
	- Filter Attendace by date periode
	- Rekap attendance by date periode
5. Modul Manage Leave (Untuk pengelolaan cuti)
6. Modul Manage Overtime (Untuk pengelolaan data lembur)
	- Overtime Request
	- Overtime OB & Driver
	- Fee (OB & Driver)
7. Modul Work Scheme (Untuk pengelolaan pola kerja karyawan)
	- Shift
	- Work Group
	- Employee Shift
8. Modul Reference
	- Branch 
	- Department
	- Position
	- Religion
	- Tax Status
9. User Privilege (For Super admin)
	- Role Management
	- Permission Management
	- User Management
10. Database Backup (For Super admin)

